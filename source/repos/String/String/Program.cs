﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace String
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 0, test = 0;
            string texte, sentence_0 = "Saisissez une phrase :", sentence_1 = "Vous avez saisi :", sentence_2="", sentence_3="",
            alpha_Maj = "ABCDEFGHIJKLMNOPQRSTUVWXYZ", alpha_Min = "abcdefghijklmnopqrstuvwxyz", point=".";
            Console.WriteLine(sentence_0);
            texte = Console.ReadLine();
            Console.WriteLine(sentence_1);
            Console.WriteLine(texte);
            while (test != 1) {
                if (alpha_Maj[count] == texte[0])
                {
                    sentence_2 = "Cette phrase commence par une majuscule.";
                    test = 1;
                }
                if (alpha_Min[count] == texte[0])
                {
                    sentence_2 = "Cette phrase commence par une minuscule.";
                    test = 1;
                }
                count++;
                if ((alpha_Maj.Length)-1 < count)
                {
                    sentence_2 = "Cette phrase ne commence pas par une minuscule ou une majuscule.";
                    test = 1;
                }
            }
            if (texte[(texte.Length)-1] == point[0])
            {
                sentence_3 = "Cette phrase se termine par un point.";
            }
            else
            {
                sentence_3 = "Cette phrase ne se termine pas par un point.";
            }
            Console.WriteLine(sentence_2);
            Console.WriteLine(sentence_3);
            Console.ReadKey();
        }
    }
}

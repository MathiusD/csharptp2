﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tableaux
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] input = new int[10];
            int test, indic;
            bool find = false;
            string sentence_0 = "Saisissez dix valeurs :", sentence_0_1=" :", sentence_1="Saisissez une valeur :",
            sentence_2_0=" est la ", sentence_2_1="-eme valeur saisie.";
            Console.WriteLine(sentence_0);
            for (int indice = 0; indice < 10; indice++)
            {
                Console.WriteLine("{0}{1}", (indice + 1), sentence_0_1);
                int.TryParse(Console.ReadLine(), out input[indice]);
            }
            Console.WriteLine(sentence_1);
            int.TryParse(Console.ReadLine(), out test);
            indic = 0;
            while (find!=true) {
                if (input[indic]==test)
                {
                    Console.WriteLine("{0}{1}{2}{3}", test, sentence_2_0, (indic + 1), sentence_2_1);
                    find = true;
                }
                indic++;
            }
            Console.ReadKey();
        }
    }
}
